/*
 *  PQ9ISH COMMS: comms module, part of an open source pocketqube stack
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef CONF_H_
#define CONF_H_

/**
 * @file conf.h
 * General configuration file
 */
#define PCB_VERSION(a,b,c) (((a) << 16) + ((b) << 8) + (c))

/*
 *  Define here your PCB version according to the tag of
 * https://gitlab.com/librespacefoundation/pq9ish/pq9ish-comms-vu-hw
 */
#define PQ_PCB_VERSION                  PCB_VERSION(0,9,6)

#ifndef PQ_PCB_VERSION
#error "The PCB version is undefined. Please use the PCB_VERSION(a,b,c) to define it. Check your version at https://gitlab.com/librespacefoundation/pq9ish/pq9ish-comms-vu-hw"
#endif

/**
 * Enable/disable the tests performed from the default task.
 * @note set it always to 0 for the FM firmware
 */
#define PQ9ISH_ENABLE_TEST               0

/**
 * Callsign for AX.25 and APRS packets
 */
#define CALLSIGN_STATION                (uint8_t*) "NOCALL"
#define CALLSIGN_DESTINATION            (uint8_t*) "NC"

/**
 * if using APRS you probobly want to choose an SSID according to http://www.aprs.org/doc/APRS101.PDF (p.93)
 */
#define SSID_STATION 					0
#define SSID_DESTINATION				0

/**
 * Serial number of the PQ9ISH satellite
 */
#define PQ9ISH_SAT_ID                    0x3C674952
#define PQ9ISH_NAME                      "PQ9ISH-1"
#define PQ9ISH_TX_FREQ_HZ                435240000
#define PQ9ISH_RX_FREQ_HZ                435240000
#define OSDLP_SCID                      1

#define PQ9ISH_DEFAULT_TX_POWER          -2
#define PQ9ISH_XTAL_FREQ_HZ              26e6
#ifndef PQ9ISH_FLASH_MAGIC_VAL
#define PQ9ISH_FLASH_MAGIC_VAL           0x7E67DF86
#endif
#define PQ9ISH_STORAGE_FLASH_ADDR        0x80fe000
/** Telemetry interval in ms */
#define PQ9ISH_TELEMETRY_INTERVAL		30000
/** Low power Telemetry interval in ms  */
#define PQ9ISH_TELEMETRY_INTERVAL_LP		60000
/**
 * Set it to 1, to bypass the PA using the TXDIFF signal path.
 * This maybe useful during testing
 */
#define PQ9ISH_BYPASS_PA                 0

/**
 * Ramp up/Ramp down period of the power amplifier in microseconds
 */
#define PWRAMP_RAMP_PERIOD_US           200

#define AX5043_RF_SWITCH_ENABLE         ANTSEL_OUTPUT_1
#define AX5043_RF_SWITCH_DISABLE        ANTSEL_OUTPUT_0

/******************************************************************************
 ****************************** Task delays ***********************************
 *****************************************************************************/

#define WDG_TASK_DELAY_MS               200

/******************************************************************************
 ****************************** RX Params **********************************
 *****************************************************************************/


#define MAX_TC_FRAME_SIZE         32

/******************************************************************************
 ****************************** OSDLP Params **********************************
 *****************************************************************************/

/**
 * enable/disable using OSDLP by defining USE_OSDLP
 */

//#define OSDLP_RX_ENABLE
//#define OSDLP_TX_ENABLE

#ifdef OSDLP_RX_ENABLE
#ifndef OSDLP_TX_ENABLE
#define OSDLP_TX_ENABLE
#endif
#endif

#define OSDLP_TC_ITEMS_PER_QUEUE        20
#define OSDLP_TM_ITEMS_PER_QUEUE        20
//#define OSDLP_MAX_TC_FRAME_SIZE         32
#define OSDLP_MAX_TC_PAYLOAD_SIZE       24
#define OSDLP_TM_FRAME_SIZE             128
#define OSDLP_TC_VCS                    5
#define OSDLP_TM_VCS                    5
#define OSDLP_MAX_TC_PACKET_LENGTH      (200 + 2)
#define OSDLP_MAX_TM_PACKET_LENGTH      (400 + 2)

/******************************************************************************
 ****************************** VC IDs **********************************
 *****************************************************************************/

#if defined(OSDLP_RX_ENABLE) || defined(OSDLP_TX_ENABLE)
#define VCID_MANAGEMENT                0
#define VCID_REG_TM                    1
#define VCID_REQ_TM                    2
#define VCID_EXPERIMENT                3
#endif /* if OSDLP_RX_ENABLE || OSDLP_TX_ENABLE*/

#endif /* CONF_H_ */
