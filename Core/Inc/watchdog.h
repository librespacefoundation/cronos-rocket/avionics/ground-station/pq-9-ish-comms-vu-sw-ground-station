/*
 *  PQ9ISH COMMS: comms module, part of an open source pocketqube stack
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WATCHDOG_H_
#define WATCHDOG_H_

#include <stdint.h>
#include "stm32l4xx_hal.h"
#include "cmsis_os.h"

#define MAX_TASKS_NUM                  32
#define CHARS_PER_TASK                 10

struct subsystem {
	uint8_t id;
	char name[CHARS_PER_TASK];
};
struct wdg_rec {
	struct subsystem  store_buffer[MAX_TASKS_NUM];
	uint32_t mask;
	uint8_t num_subsystems;
};

struct watchdog {
	uint8_t               subsystems_num;
	uint32_t              subsystems;
	uint32_t              subsystems_mask;
	uint8_t               registered;
	osMutexId             mtx;
	struct wdg_rec        *wdg_recorder;
	IWDG_HandleTypeDef    *hiwdg;
};

#define TOTAL_RAM_USED MAX_TASKS_NUM * sizeof(struct subsystem) + sizeof(uint32_t) + sizeof(uint8_t)

int
watchdog_init(struct watchdog *w, IWDG_HandleTypeDef *hiwdg,
              osMutexId mtx, uint8_t n, struct wdg_rec *wdg_recorder,
              uint8_t *mem_addr);

int
watchdog_register(struct watchdog *w, uint8_t *id, const char *name);

int
watchdog_reset(struct watchdog *w);

int
watchdog_reset_subsystem(struct watchdog *w, uint8_t id);


#endif /* WATCHDOG_H_ */
