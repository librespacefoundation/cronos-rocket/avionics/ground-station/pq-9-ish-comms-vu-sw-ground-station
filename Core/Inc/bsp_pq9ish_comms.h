/*
 *  PQ9ISH COMMS: comms module, part of an open source pocketqube stack
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <Drivers/AX5043/include/ax5043.h>
#include "conf.h"

#if PQ_PCB_VERSION >= PCB_VERSION(0,9,3)
#define PA_ENABLE           1
#define PA_DISABLE          0
//#define RF_SWITCH_ENABLE    1

#elif PQ_PCB_VERSION >= PCB_VERSION(0,9,2)
#define PA_ENABLE   0
#define PA_DISABLE  1

#elif PQ_PCB_VERSION >= PCB_VERSION(0,9,0)
#define PA_ENABLE   0
#define PA_DISABLE  1
#undef AX5043_IRQ_Pin
#define AX5043_IRQ_Pin GPIO_PIN_9
#undef AX5043_IRQ_GPIO_Port
#define AX5043_IRQ_GPIO_Port GPIOA
#undef AX5043_IRQ_EXTI_IRQn
#define AX5043_IRQ_EXTI_IRQn EXTI9_5_IRQn
#endif

typedef enum {
	POWER_OFF = (uint8_t) 0,
	POWER_ON,
	POWER_STANBY
} pq9ish_can_power_t;

void
enable_pa(struct ax5043_conf *hax);

void
disable_pa(struct ax5043_conf *hax);

void
pq9ish_can_power(pq9ish_can_power_t state);
